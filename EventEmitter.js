var events = require('events');
var util = require('util');

var myEmitter = new events.EventEmitter();

myEmitter.on('someEvent', function (data){
	console.log('Event says ' + data);
	});

myEmitter.emit('someEvent','Hi');


// custom events 

var person = function(name, age){
	this.name = name;
	this.age = age;
	};

util.inherits(person, events.EventEmitter);

var kartik = new person('kartik', 26);
var purva = new person('purva', 25);

var persons = [kartik, purva];
persons.forEach(function(person){

person.on('speak', function(data){
	console.log(person.name +' syas : '+ data);
	});
	});


kartik.emit('speak', 'Hi Purva! How r u');
purva.emit('speak', 'Hi Kartik! I am fine. How r u');
