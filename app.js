var http = require('http');
var fs = require('fs');

var serverAddr = '127.0.0.1';
var serverPort = 3000;

var server = http.createServer(function(req, res){
	console.log(req.url);
	
	var readStream = fs.createReadStream(__dirname+'/HTML.html', 'utf8');
	//res.writeHead(200, {'content-Type' : 'text/html'});
	readStream.pipe(res);
	//res.end('<h1>Love you NodeJs</h1>')
	});
	
server.listen(serverPort, serverAddr);
console.log('Server Listening on port 3000');
