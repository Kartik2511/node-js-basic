var fs = require('fs');
/*
var read = fs.readFile('FileIO.txt', 'utf8', function(err, chunk){
	fs.writeFile('writeMe', chunk, function(){
		console.log(chunk);
		});
	});

console.log('reading');
*/

var readStream = fs.createReadStream(__dirname+'/FileIO.txt', 'utf8');

var writeStream=fs.createWriteStream(__dirname+'/WriteIO.txt');

readStream.on('data', function(chunk){
	console.log('Reading................');
	writeStream.write(chunk);
	});

readStream.pipe(writeStream); // pipe does same as above 'data' event on readablke stream


